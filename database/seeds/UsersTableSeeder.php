<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

/**
 * Class UserTableSeeder.
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     */
    public function run()
    {

        User::create([
            'name' => 'user1',
            'email' => 'user1@user.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret1'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name' => 'user2',
            'email' => 'user2@user.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret2'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name' => 'user3',
            'email' => 'user3@user.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret3'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name' => 'user4',
            'email' => 'user4@user.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret4'),
            'remember_token' => Str::random(10),
        ]);
    }
}
