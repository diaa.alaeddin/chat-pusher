# laravel-chat

Build a chat app with Laravel, Vue.js and Pusher. Follow the tutorial [https://pusher.com/tutorials/chat-laravel](https://pusher.com/tutorials/chat-laravel)

## Screenshots for execution

![Screenshot](resources/media/main chat.png?raw=true "Screenshot")
![Screenshot](resources/media/priveate chat.png?raw=true "Screenshot")
![Screenshot](resources/media/scheduled message.png?raw=true "Screenshot")

## Getting Started

```bash
composer install
```

```bash
npm install
```

Duplicate `.env.example` and rename it `.env`

Then run:

```bash
php artisan key:generate
```

### Prerequisites

#### Setup Pusher

If you don't have one already, create a free Pusher account at [https://pusher.com/signup](https://pusher.com/signup) then login to your dashboard and create an app.

Set the `BROADCAST_DRIVER` in your `.env` file to **pusher**:

```txt
BROADCAST_DRIVER=pusher
```

Then fill in your Pusher app credentials in your `.env` file:

```txt
PUSHER_APP_ID=xxxxxx
PUSHER_APP_KEY=xxxxxxxxxxxxxxxxxxxx
PUSHER_APP_SECRET=xxxxxxxxxxxxxxxxxxxx
PUSHER_APP_CLUSTER=
```

#### Database Migrations

Be sure to fill in your database details in your `.env` file before running the migrations:

```bash
php artisan migrate
```

#### Database Seed

Use this command to run seeder:

```bash
php artisan db:seed
```

And finally, start the application:

```bash
php artisan serve
```

and visit [http://localhost:8000/](http://localhost:8000/) to see the application in action.

#### scheduled Message 

To test send scheduled messages every five minutes, manually you can use this command in new Terminal after 'php artisan serve':

```bash
php artisan notification:sendMessages
```

or you can follow this link to create laravel task like (Corn) on windows  [https://quantizd.com/how-to-use-laravel-task-scheduler-on-windows-10/](https://quantizd.com/how-to-use-laravel-task-scheduler-on-windows-10/)

## Built With

* [Pusher](https://pusher.com/) - APIs to enable devs building realtime features
* [Laravel](https://laravel.com) - The PHP Framework For Web Artisans
* [Vue.js](https://vuejs.org) - The Progressive JavaScript Framework
