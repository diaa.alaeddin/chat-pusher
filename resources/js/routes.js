import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter);

// components
import communicate from './components/chat/Communicate'

const routes = [
    {path: '/chat/:id', component: communicate},
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router;


