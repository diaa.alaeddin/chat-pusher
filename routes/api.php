<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth'], function () {

    Route::get('/user', 'UserController@me');
    Route::get('/users/{id}', 'UserController@getUserInfo');

    Route::post('/message/send', 'ChatController@sendMessage');
    Route::get('/message/{id}', 'ChatController@getMessagesAuthToUser');
    Route::post('/read/message/{id}', 'ChatController@readMessage');
    Route::post('/read/messages/{id}', 'ChatController@readMessages');
});


