<?php


namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;


abstract class BaseTest extends TestCase
{
    use DatabaseMigrations;

    protected $user1 = null;
    protected $user2 = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed', ['--class' => 'UsersTableSeeder']);

        $this->user1 = new User([
            'name' => 'test1',
            'email' => 'test1@test.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret1'),
            'remember_token' => Str::random(10),
        ]);
        $this->user1->save();
        $this->user2 = new User([
            'name' => 'test2',
            'email' => 'test2@test.com',
            'email_verified_at' => now(),
            'password' => bcrypt('secret2'),
            'remember_token' => Str::random(10),
        ]);
        $this->user2->save();
    }

    public function getUser1()
    {
        return $this->user1;
    }

    public function getUser2()
    {
        return $this->user2;
    }

}