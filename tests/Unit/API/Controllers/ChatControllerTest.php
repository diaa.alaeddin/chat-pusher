<?php

namespace Tests\unit\API\Controllers;

use App\User;
use App\Message;
use Carbon\Carbonite;
use Hash;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\BaseTest;

class ChatControllerTest extends BaseTest
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testSendMessage()
    {
        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $message = new Message();
        $message->message = 'Hello';
        $message->from_user_id = $user1->id;
        $message->to_user_id = $user2->id;
        $response = $this->actingAs($user1)
            ->post('/api/message/send', $message->toArray())
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    public function testGetMessagesAuthToUser()
    {
        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $message = new Message();
        $message->message = 'Hello';
        $message->from_user_id = $user1->id;
        $message->to_user_id = $user2->id;
        $data = Message::where(['to_user_id' => $user2->id, 'from_user_id' => $user1->id])->orWhere('from_user_id', $user2->id)->where('to_user_id', $user1->id)->get();
        $response = $this->actingAs($user1)
            ->get('/api/message/' . $user1->id)
            ->assertStatus(200);
    }


    public function testReadMessage()
    {
        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $message = new Message();
        $message->message = 'Hello';
        $message->from_user_id = $user1->id;
        $message->to_user_id = $user2->id;
        $message->save();
        $response = $this->actingAs($user1)
            ->post('/api/read/message/' . $message->id)
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }

    public function testReadMessages()
    {
        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $message = new Message();
        $message->message = 'Hello';
        $message->from_user_id = $user1->id;
        $message->to_user_id = $user2->id;
        $message->save();
        $response = $this->actingAs($user1)
            ->post('/api/read/messages/' . $user1->id)
            ->assertStatus(200)
            ->assertJson(['status' => 'success']);
    }
}
