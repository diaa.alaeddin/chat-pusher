<?php

namespace Tests\unit\API\Controllers;

use App\User;
use App\Message;
use Carbon\Carbonite;
use Hash;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\BaseTest;

class UserControllerTest extends BaseTest
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }


    public function testMe()
    {
        $user1 = $this->getUser1();

        $response = $this->actingAs($user1)
            ->get('/api/user')
            ->assertStatus(200)
            ->assertJson($user1->toArray());
    }

    public function testGetUserInfo()
    {
        $user1 = $this->getUser1();
        $user2 = $this->getUser2();
        $response = $this->actingAs($user1)
            ->get('/api/users/' . $user2->id)
            ->assertStatus(200)
            ->assertJson(User::where('id', '=', $user2->id)->get()->toArray());
    }
}
