<?php

namespace App\Console\Commands;

use App\Events\MessageSent;
use App\Message;
use App\User;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class sendScheduledMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:sendMessages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send scheduled messages to all users every five minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('id', '!=', 1)->get();
        $users->map(function ($user) {
            $message = new Message();
            $message->message = 'this message is scheduled message';
            $message->from_user_id = 1;
            $message->to_user_id = $user->id;
            $message->save();
            event(new MessageSent($user, $message));
        });
    }
}
