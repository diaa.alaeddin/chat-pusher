<?php

namespace App\Http\API\Controllers;

use App\Events\MessageSent;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\API\Requests\SendMessageRequest;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * send message
     *
     * returns response
     *
     * @Post("/message/send")
     * @Request(
     *     body={
     *          "message": "message content",
     *          "to_user_id": "to user id",
     *     },
     *     attributes={
     *          @Attribute("message", description="User Message Content"),
     *          @Attribute("to_user_id", description="Reciver User's ID"),
     *      }
     *  )
     * @Response(200)
     */
    public function sendMessage(SendMessageRequest $request)
    {
        try {
            $user = Auth::user();
            $message = new Message();
            $message->message = $request->message;
            $message->to_user_id = $request->to_user_id;
            $message->from_user_id = $user->id;
            $message->save();
            event(new MessageSent($user, $message));
            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error']);
        }
    }

    /**
     * Get all user's messages
     *
     * returns list of chat history
     *
     * @Get("/message/{id}")
     * @Response(200)
     */
    public function getMessagesAuthToUser($id)
    {
        $user_id = auth()->user()->id;
        $data = Message::where(['to_user_id' => $id, 'from_user_id' => $user_id])->orWhere('from_user_id', $id)->where('to_user_id', $user_id)->get();
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /**
     * Make user's message as read based on ID
     *
     * @Post("/message/{id}")
     * @Parameters({
     *      @Parameter("id", description="Id of the user to be returned", required=True)
     * })
     * @Response(200, body={
     *     "status":"success",
     *     },
     *     attributes={
     *          @Attribute("status", type="string", description="request status", required=true),
     *     }
     *     )
     */
    public function readMessage($id)
    {

        $msg = Message::find($id);
        if (empty($msg)) {
            return response()->json(['status' => 'error']);
        }
        $msg->is_read = 1;
        $msg->update();
        return response()->json(['status' => 'success']);
    }

    /**
     * Make all user's messages as read based on ID
     *
     * @Post("/messages/{id}")
     * @Parameters({
     *      @Parameter("id", description="Id of the user to be returned", required=True)
     * })
     * @Response(200, body={
     *     "status":"success",
     *     },
     *     attributes={
     *          @Attribute("status", type="string", description="request status", required=true),
     *     }
     *     )
     */
    public function readMessages($id)
    {
        $msg = Message::where('from_user_id', $id);
        if (empty($msg)) {
            return response()->json(['status' => 'error']);
        }
        $msg->update(['is_read' => 1]);
        return response()->json(['status' => 'success']);
    }
}
