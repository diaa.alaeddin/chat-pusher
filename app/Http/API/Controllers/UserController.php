<?php

namespace App\Http\API\Controllers;

use App\Events\MessageSent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\API\Requests\SendMessageRequest;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get current user info
     *
     * returns current user info
     *
     * @Get("/user")
     * @Response(200)
     */
    public function me()
    {
        return Auth::user();
    }

    /**
     * Get user info
     *
     * returns user info
     *
     * @Get("/users/{id}")
     * @Response(200)
     */
    public function getUserInfo($id)
    {
        return User::where('id', '=', $id)->get()->toArray();
    }

}
