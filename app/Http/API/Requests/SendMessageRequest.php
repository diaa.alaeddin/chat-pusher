<?php

namespace App\Http\API\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'required',
            'to_user_id' => 'required|exists:users,id',
        ];
    }
}
